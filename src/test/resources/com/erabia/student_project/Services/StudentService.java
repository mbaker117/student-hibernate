package com.erabia.student_project.Services;


import java.util.List;
import java.util.Optional;

import com.erabia.csvfile.exception.CSVFileException;
import com.erabia.student_hibernate.exception.StudentHibernateException;
import com.erabia.student_project.bean.Student;
import com.erabic.StudentDAOExample.exception.StudentDAOException;



public interface StudentService {
	
	public void add(Student std) throws StudentDAOException, CSVFileException;

	public void update(int id, Student std) throws StudentDAOException, CSVFileException;

	public void delete(int id) throws StudentDAOException, CSVFileException, StudentHibernateException;

	public Optional<Student> get(int id) throws  StudentDAOException, CSVFileException, StudentHibernateException;

	public Optional<List<Student>> getAll() throws  StudentDAOException, CSVFileException;
	
}
