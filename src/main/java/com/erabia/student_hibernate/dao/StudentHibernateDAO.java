package com.erabia.student_hibernate.dao;


import java.util.List;
import java.util.Optional;

import com.erabia.student_hibernate.bean.Student;
import com.erabia.student_hibernate.exception.StudentHibernateException;



public interface StudentHibernateDAO {
	public Student add(Student std);

	public Student update( Student std) ;

	public void delete(int id) throws StudentHibernateException ;

	public Optional<Student> get(int id) throws StudentHibernateException ;

	public Optional<List<Student>> getAll() ;

}
