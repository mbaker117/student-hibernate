package com.erabia.student_hibernate.dao.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;



import org.hibernate.Session;

import org.hibernate.Transaction;

import org.hibernate.query.Query;

import com.erabia.student_hibernate.bean.Student;
import com.erabia.student_hibernate.dao.StudentHibernateDAO;
import com.erabia.student_hibernate.exception.StudentHibernateException;
import com.erabia.student_hibernate.exception.enums.StudentHibernateExceptionType;
import com.erabia.student_hibernate.util.HibernateUtil;

public class StudentHibernateDAOImpl implements StudentHibernateDAO {

	private HibernateUtil hibernateUtil;

	public StudentHibernateDAOImpl() throws StudentHibernateException {
		try {
			Constructor<HibernateUtil> declaredConstructor = HibernateUtil.class.getDeclaredConstructor();
			declaredConstructor.setAccessible(true);
			hibernateUtil = declaredConstructor.newInstance();
			declaredConstructor.setAccessible(false);

		} catch (NoSuchMethodException e) {

			throw new StudentHibernateException(StudentHibernateExceptionType.NO_SUCH_METHOD_EXCEPTION,
					StudentHibernateExceptionType.NO_SUCH_METHOD_EXCEPTION.getMsg(), null);
		} catch (SecurityException e) {

			throw new StudentHibernateException(StudentHibernateExceptionType.SECURITY_EXCEPTION,
					StudentHibernateExceptionType.SECURITY_EXCEPTION.getMsg(), null);
		} catch (InstantiationException e) {

			throw new StudentHibernateException(StudentHibernateExceptionType.INSTANTIATION_EXCEPTION,
					StudentHibernateExceptionType.INSTANTIATION_EXCEPTION.getMsg(), null);
		} catch (IllegalAccessException e) {

			throw new StudentHibernateException(StudentHibernateExceptionType.ILLEGAL_ACCESS_EXCEPTION,
					StudentHibernateExceptionType.ILLEGAL_ACCESS_EXCEPTION.getMsg(), null);
		} catch (IllegalArgumentException e) {
			throw new StudentHibernateException(StudentHibernateExceptionType.ILLEGAL_ARGUMENT_EXCEPTION,
					StudentHibernateExceptionType.ILLEGAL_ARGUMENT_EXCEPTION.getMsg(), null);
		} catch (InvocationTargetException e) {
			throw new StudentHibernateException(StudentHibernateExceptionType.INVOCATION_TARGET_EXCEPTION,
					StudentHibernateExceptionType.INVOCATION_TARGET_EXCEPTION.getMsg(), null);
		}

	}

	public Student add(Student std) {
		// TODO Auto-generated method stub
		if (std == null)
			throw new IllegalArgumentException("student is null");
		Session session = hibernateUtil.getSession();

		Transaction t = session.beginTransaction();

		session.save(std);
		t.commit();
		System.out.println("successfully saved");
		session.close();
		return std;
		

	}

	public Student update(Student std) {
		if (std == null)
			throw new IllegalArgumentException("student is null");
		Session session = hibernateUtil.getSession();
		session.beginTransaction();
		session.update(std);

		session.getTransaction().commit();

		session.close();
		return std;

	}

	public void delete(int id) throws StudentHibernateException {
		Session session = hibernateUtil.getSession();
		session.beginTransaction();
		Optional<Student> optional = get(id);
		if (!optional.isPresent())
			throw new StudentHibernateException(StudentHibernateExceptionType.NO_EXISTING_STUDENT,
					StudentHibernateExceptionType.NO_EXISTING_STUDENT.getMsg(), null);
		session.delete(optional.get());
		session.getTransaction().commit();
		session.close();

	}

	public Optional<Student> get(int id)  {

		Session session = hibernateUtil.getSession();
		session.beginTransaction();
		Student student = session.get(Student.class, id);
	

		session.getTransaction().commit();

		session.close();
		return Optional.ofNullable(student);
	}

	public Optional<List<Student>> getAll() {
		Session session = hibernateUtil.getSession();
		Query q = session.createQuery("from Student");
		List<Student> students = q.getResultList();

		return Optional.ofNullable(students);
	}

}
