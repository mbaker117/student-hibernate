package com.erabia.student_hibernate.test;

import java.util.Optional;

import com.erabia.student_hibernate.bean.Student;
import com.erabia.student_hibernate.dao.StudentHibernateDAO;
import com.erabia.student_hibernate.dao.impl.StudentHibernateDAOImpl;
import com.erabia.student_hibernate.exception.StudentHibernateException;

public class Client {
	public static void main(String [] arg) throws StudentHibernateException {
		StudentHibernateDAO st = new StudentHibernateDAOImpl();
		
		/* st.add(new Student("hib","test",100)); */
		st.getAll();
	}
}
