package com.erabia.student_hibernate.bean;
import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="students")
public class Student implements Serializable {
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	@Column(name ="id")
	private int id;
	@Column(name ="FirstName")
	private String firstName;
	@Column (name = "LastName")
	private String lastName;
	@Column(name ="Avg")
	private double avg;
	public Student() {	}
	public Student(String firstName, String lastName, double avg) {
		
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.avg = avg;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public double getAvg() {
		return avg;
	}
	public void setAvg(double avg) {
		this.avg = avg;
	}
	@Override
	public String toString() {
		return "Student [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", avg=" + avg + "]";
	}
	
	

}
